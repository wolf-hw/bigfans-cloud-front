/**
 * Created by lichong on 3/5/2018.
 */


import React from 'react';
import {Form, Input , Checkbox , Button , Modal , message} from 'antd'
import CitySwitcher from '../CitySwitcher'
import HttpUtils from 'utils/HttpUtils'

const FormItem = Form.Item;

class AddressForm extends React.Component {

    constructor(props){
        super(props);
    }

    componentDidMount () {
    }

    handleSubmit (e) {
        e.preventDefault();
        let self = this;
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                let newAddress = this.props.form.getFieldsValue();
                HttpUtils.createAddress(newAddress , {
                    success(resp){
                        self.props.handleOk(resp.data);
                    },
                    error(resp){
                        message.error(resp.message);
                    }
                })
            }
        });
    }

    formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 6 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 18 },
        },
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Modal
                title={this.props.title}
                visible={this.props.visible}
                onOk={(e) => this.handleSubmit(e)}
                onCancel={this.props.handleCancel}
                footer={null}
            >
                <div style={this.props.style}>
                    <FormItem {...this.formItemLayout} label="收货人" hasFeedback>
                        {getFieldDecorator('consignee', {
                            rules: [{message: 'Please input your nickname!', whitespace: true}],
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem {...this.formItemLayout} label="配送区域" hasFeedback>
                        {getFieldDecorator('area', {
                            rules: [{message: '请选择配送区域!', whitespace: false}],
                        })(
                            <CitySwitcher/>
                        )}
                    </FormItem>
                    <FormItem {...this.formItemLayout} label="详细地址" hasFeedback>
                        {getFieldDecorator('detailAddress')(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem {...this.formItemLayout} label="手机" hasFeedback>
                        {getFieldDecorator('mobile')(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem {...this.formItemLayout} label="邮箱" hasFeedback>
                        {getFieldDecorator('email')(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem {...this.formItemLayout}>
                        {getFieldDecorator('isPrimary', {
                            valuePropName: 'checked',
                        })(
                            <Checkbox>保存为默认地址</Checkbox>
                        )}
                    </FormItem>
                    <FormItem {...this.formItemLayout}>
                        {getFieldDecorator('isPrimary', {
                            valuePropName: 'checked',
                        })(
                            <Button onClick={(e) => this.handleSubmit(e)}>保存</Button>
                        )}
                    </FormItem>
                </div>
            </Modal>
        );
    }
}

export default Form.create()(AddressForm);