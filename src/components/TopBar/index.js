/**
 * Created by lichong on 2/14/2018.
 */

import React, {Component} from 'react';
import {Icon, Menu} from 'antd';

import {Link} from 'react-router-dom'

class TopBar extends Component {
    render() {
        return (
            <div>
                <div className="pull-left">
                    <Menu
                        mode="horizontal"
                    >
                        <Menu.Item key="home">
                            <Link to="/">首页</Link>
                        </Menu.Item>
                    </Menu>
                </div>
                <div className="pull-right">
                    <Menu
                        mode="horizontal"
                    >
                        <Menu.Item key="login">
                            <Link to="/login"><Icon type="appstore"/>登录</Link>
                        </Menu.Item>
                        <Menu.Item key="mail">
                            <Icon type="mail"/>大蕃薯
                        </Menu.Item>
                        <Menu.Item key="app">
                            <Link to="/myorders"><Icon type="appstore"/>我的订单</Link>
                        </Menu.Item>
                        <Menu.Item key="center">
                            <a href="/center"><Icon type="appstore"/>个人中心</a>
                        </Menu.Item>
                    </Menu>
                </div>
            </div>
        );
    }
}

export default TopBar;