import React, {Component} from 'react';

import {Row, Col , Divider , Steps , Card , Radio , Button} from 'antd';
import TopBar from 'components/TopBar';
import ReceiverSelector from '../../components/ReceiverSelector';
import PaymethodSelector from '../../components/PaymethodSelector';
import OrderItems from './OrderItems';
import Property from './Property'

import HttpUtils from '../../utils/HttpUtils'

const Step = Steps.Step;

class Detail extends Component {

    state = {
        order : {},
        receiver : {},
        payMethodCode : '',
        submitting : false
    }

    submit () {
        let self = this;
        // this.setState({submitting: true})
        let order = this.state.order;
        order.addressId = this.state.receiver.id;
        order.payMethodCode = this.state.payMethodCode;
        HttpUtils.createOrder(order , {
            success(resp) {
                if(resp.status == 200 && resp.data){
                    self.props.history.push("/pay?orderId=" + resp.data);
                }
            }
        })
    }

    setOrderInfo(order){
        this.setState({order})
    }

    setReceiver(receiver){
        this.setState({receiver})
    }

    setPayMethod(payMethodCode){
        this.setState({payMethodCode})
    }

    render() {
        return (
            <div>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <TopBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Divider style={{margin: '0'}}/>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <Steps current={1} size="small" style={{marginTop : '20px'}}>
                            <Step title="我的购物车" />
                            <Step title="确认订单" />
                            <Step title="支付完成" />
                        </Steps>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <h3 style={{marginTop : '30px'}}>填写并核对订单信息</h3>
                        <Card style={{marginTop : '10px'}}>
                            <ReceiverSelector setReceiverInfo={(r) => this.setReceiver(r)}/>
                            <PaymethodSelector setPayMethod={(p) => this.setPayMethod(p)}/>
                            <OrderItems setOrderInfo={(o) => this.setOrderInfo(o)}/>
                            <Property/>
                            <div>
                                <Row>
                                    <Col span={5} offset={16}>
                                        <p className='pull-right' style={{margin:'0px'}}><span style={{color:'red'}}>{this.state.order.totalQuantity}</span> 件商品，总商品金额:</p>
                                    </Col>
                                    <Col span={3}>
                                        <span className='pull-right' style={{marginRight:'8px'}}>{this.state.order.originalTotalPrice}</span>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col span={5} offset={16}>
                                        <p className='pull-right' style={{margin:'0px'}}>运费:</p>
                                    </Col>
                                    <Col span={3}>
                                        <span className='pull-right' style={{marginRight:'8px'}}>{this.state.order.freight || 0.00}</span>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col span={5} offset={16}>
                                        <p className='pull-right' style={{margin:'0px'}}>优惠:</p>
                                    </Col>
                                    <Col span={3}>
                                        <span className='pull-right' style={{marginRight:'8px'}}>-{this.state.order.savedMoney || 0.00}</span>
                                    </Col>
                                </Row>
                            </div>
                            <div style={{'background': '#f4f4f4'}}>
                                <Row style={{marginRight:'8px'}}>
                                    <Col>
                                        <span className="pull-right">应付金额： ¥ <span style={{color:'red'}}>{this.state.order.totalPrice}</span> </span>
                                        <br/>
                                        <p className="pull-right" style={{'padding': '5px'}}>
                                            <span>寄送至： {this.state.receiver.detailAddress}</span>
                                            <span style={{marginLeft:'10px'}}>收货人：{this.state.receiver.consignee}</span>
                                            <span style={{marginLeft:'10px'}}>{this.state.receiver.mobile}</span>
                                        </p>
                                    </Col>
                                </Row>
                                <Row style={{marginRight:'8px'}}>
                                    <Col span={3} className="pull-right">
                                        <Button type="primary" size='large' loading={this.state.submitting} className="pull-right" onClick={() => this.submit()}>提交订单</Button>
                                    </Col>
                                </Row>
                            </div>
                        </Card>
                    </Col>
                    <Col span={3}/>
                </Row>
            </div>
        )
    }
}

export default Detail;