import React from 'react';

import {Table, Row, Col} from 'antd';

import HttpUtils from '../../../utils/HttpUtils'

const tableProps = {
    bordered: false,
    loading: false,
    pagination: false,
    size: 'default',
    showHeader: true,
    scroll: undefined,
}

class OrderItems extends React.Component {

    columns = [{
        title: '商品',
        dataIndex: 'name',
        key: 'name',
        width: 400,
        render: (text, record) => this.renderProduct(text, record)
    }, {
        title: '单价',
        dataIndex: 'age',
        key: 'age',
        width: 100,
        render: (text, record) => this.renderPrice(text, record)
    }, {
        title: '优惠',
        dataIndex: 'promotion',
        key: 'promotion',
        width: 150,
        render: (text, record) => this.renderPromotion(text, record)
    }, {
        title: '数量',
        dataIndex: 'address',
        key: 'address',
        width: 150,
        render: (text, record) => this.renderQuantity(text, record)
    }, {
        title: '小计',
        dataIndex: 'subtotal',
        key: 'subtotal',
        width: 100,
        render: (text, record) => this.renderSubtotal(text, record)
    }];

    state = {
        order: {
            items: []
        }
    }

    componentDidMount() {
        let self = this;
        HttpUtils.checkout({} , {
            success (resp) {
                if(resp.status == 200 && resp.data){
                    self.setState({order : resp.data});
                    self.props.setOrderInfo(resp.data)
                }
            } ,
            error () {

            }
        })
    }

    render() {
        return (
            <div>
                <Row>
                    <Col span={3}>
                        <h3>确认订单信息</h3>
                    </Col>
                </Row>
                <Row>
                    <Table {...tableProps} columns={this.columns} dataSource={this.state.order.items}/>
                </Row>
            </div>
        );
    }

    renderProduct(text, record) {
        return (
            <div>
                <Col span={9}>
                    <a href={'/product/' +record.prodId } target='_blank'>
                        <img width="120" src={record.prodImg}/>
                    </a>
                </Col>
                <Col span={8}>
                    <a href={'/product/' +record.prodId } target='_blank'>
                        <span className="ant-form-text" style={{fontSize: '14px'}}>{record.prodName}</span><br/>
                    </a>
                </Col>
                <Col span={7}>
                    {
                        record.specs.map((spec , index) => {
                            return (
                                <div key={index}>
                                    <span className="ant-form-text"
                                          style={{fontSize: '10px', marginLeft: '10px', color: '#666'}}>{spec.option}: {spec.value}</span><br/>
                                </div>
                                )
                        })
                    }
                </Col>
            </div>
        );
    }

    renderPrice(text, record) {
        return (
            <div>
                <span className="ant-form-text">{record.price}</span><br/>
            </div>
        );
    }

    renderPromotion(text, record) {
        return (
            <div>
                <span className="ant-form-text">
                    {
                        record.promotions
                        &&
                        record.promotions.map((pmt , index) => {
                            return (
                                <div key={index}>
                                    <span>{pmt.promotionName}</span>
                                </div>
                            )
                        })
                    }</span><br/>
            </div>
        );
    }

    renderQuantity(text, record) {
        return (
            <div>
                <span className="ant-form-text">x {record.quantity}</span><br/>
            </div>
        )
    }

    renderSubtotal(text, record) {
        return (
            <div>
                {
                    record.originalSubTotal == record.subTotal ?
                        (
                            <div>
                            <span className="ant-form-text"
                                  style={{fontSize: '10px', marginLeft: '10px', color: '#666'}}>¥{record.subTotal}</span><br/>
                            </div>
                        )
                        :
                        (
                            <div>
                                <del className="ant-form-text"
                                      style={{fontSize: '10px', marginLeft: '10px', color: '#666'}}>¥{record.originalSubTotal}</del><br/>
                            <span className="ant-form-text"
                                  style={{fontSize: '10px', marginLeft: '10px', color: '#666'}}>¥{record.subTotal}</span><br/>
                            </div>
                        )
                }

            </div>
        )
    }
}
export default OrderItems;